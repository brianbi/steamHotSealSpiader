const request = require('request');
const cheerio = require('cheerio');
const xlsx 	  = require('node-xlsx');
const fs 	  = require('fs');
const ProgressBar = require('./lib/progress-bar');

function _request(opts) {
	return new Promise ((resolve, reject) => {
        request.post(opts, function(err, response, body) {
            if(err) {reject(err)} else { resolve(body) }
        });    
    });
}

async function getHotSeal (cc, filePath, totalPage) {
	let gameData = [["AppName", "AppID", "BandelID", "Price"]];
	let pb = new ProgressBar('下载进度', totalPage);

	console.time('download current page use time:');

	for (let i = 1; i <= totalPage; i++)
	{
		pb.render({ completed: i, total: totalPage });
		let urlStr = 'https://store.steampowered.com/search/?filter=topsellers&cc=' + cc + '&page=' + i;
		let result = await _request({url: urlStr});

		let $ = cheerio.load(result);

		$('a.search_result_row').each((index, element) => {
			let appId = $(element).attr('data-ds-appid');
			let bandelId = $(element).attr('data-ds-bundleid');
			let name  = $(element).children('div.responsive_search_name_combined').children('div.search_name').children('span.title').text();
			let price = $(element).children('div.responsive_search_name_combined').children('div.search_price_discount_combined')
						.children('div.search_price ').clone().children().remove().end().text();

			gameData.push([name, appId, bandelId, price]);			
		});
	}

	let buffer = xlsx.build([{name: "sheet1", data: gameData}]);
	fs.writeFileSync(filePath, buffer, {'flag': 'w'});

	console.timeEnd('download current page use time:');
}

let cc = 'no';
let totalPage= 5;
let filePath = './xlsx/hotSeal_' + cc + '.xlsx';

fs.exists('./xlsx', (exists) => {
	if (exists)
	{
		fs.exists(filePath, (exists) => {
			if (exists) fs.unlinkSync(filePath);
		});
	} else {
		fs.mkdir('./xlsx', () => {});
	}
});


getHotSeal(cc, filePath, totalPage);
